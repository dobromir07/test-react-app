module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "overrides": [
    ],
    "parser": "@babel/eslint-parser",
       "settings": {
        "react": {
            "version": "detect"
        }
    }, 
    "extends": [
        "plugin:react/recommended",
        "plugin:react/jsx-runtime",
//        "standard"
//        "plugin:react/jsx-runtime",
//        "plugin:testing-library/react",
//        "plugin:jest/all"
    ],
    "parserOptions": {
        "requireConfigFile": false, 
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "jsx-a11y",
//         "jsx", 
//        "flow", 
//        "typescript"
    ],
    "rules": {
    }
}
